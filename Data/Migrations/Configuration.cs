﻿namespace Data.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Model.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Data.WebDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Data.WebDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new WebDbContext()));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new WebDbContext()));
            var user = new ApplicationUser()
            {
                UserName = "user1",
                Email = "thanhnguyen@gmail.com",
                EmailConfirmed = true, 
                BirthDay = DateTime.Now,
                FullName = "Thanh Nguyen"
            };
            manager.Create(user, "abc@123");
            //Nếu role chưa tồn tại thì tạo role
            if (!roleManager.Roles.Any())
            {
                roleManager.Create(new IdentityRole { Name = "Admin" });
                roleManager.Create(new IdentityRole { Name = "User" });
            }
            //Dùng userRole để tìm theo email
            var adminUser = manager.FindByEmail("thanhnguyen@gmail.com");
            //Nếu tìm thành công thì sẽ add vào 2 nhóm role
            manager.AddToRoles(adminUser.Id, new string[] { "Admin", "User" });
        }
    }
}
