namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateproduct : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "CarModel", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "CarModel");
        }
    }
}
