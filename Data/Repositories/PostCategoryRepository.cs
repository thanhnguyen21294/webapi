﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IPostCategoryRepository: IRepository<PostCategory>
    {
        IEnumerable<PostCategory> GetAllByTagPaging(string tag, int pageIndex, int pageSize, out int totalRow);
    }
    public class PostCategoryRepository: RepositoryBase<PostCategory>, IPostCategoryRepository
    {
        public PostCategoryRepository(IDbFactory dbFactory): base(dbFactory)
        {

        }

        public IEnumerable<PostCategory> GetAllByTagPaging(string tag, int pageIndex, int pageSize, out int totalRow)
        {
            var query = from pc in DbContext.PostCategories
                        join pt in DbContext.PostTags
                        on pc.ID equals pt.PostID
                        where pt.TagID == tag && pc.Status
                        select pc;
            totalRow = query.Count();
            query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            return query;
        }
    }
}
