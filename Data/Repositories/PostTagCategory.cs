﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IPostTagCategory: IRepository<PostTag>
    {

    }
    public class PostTagCategory: RepositoryBase<PostTag>, IPostTagCategory
    {
        public PostTagCategory(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
