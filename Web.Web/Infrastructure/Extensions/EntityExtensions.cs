﻿using Microsoft.Ajax.Utilities;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Web.Controllers;
using Web.Web.Models;

namespace Web.Web.Infrastructure.Extensions
{
    public static class EntityExtensions
    {
        public static void UpdatePostCategory(this PostCategory postCategory, PostCategoryViewModel postCategoryVM)
        {
            postCategory.Name = postCategoryVM.Name;
            postCategory.Alias = postCategoryVM.Alias;
            postCategory.DisplayOrder = postCategoryVM.DisplayOrder;
            postCategory.ParentID = postCategoryVM.ParentID;
            postCategory.Image = postCategoryVM.Image;
            postCategory.HomeFlag = postCategoryVM.HomeFlag;
            postCategory.CreatedDate = postCategoryVM.CreatedDate;
            postCategory.CreatedBy = postCategoryVM.CreatedBy;
            postCategory.UpdateDate = postCategoryVM.UpdateDate;
            postCategory.UpdateBy = postCategoryVM.UpdateBy;
            postCategory.MetaKeyword = postCategoryVM.MetaKeyword;
            postCategory.MetaDescription = postCategoryVM.MetaDescription;
            postCategory.Status = postCategoryVM.Status;
        }

        public static void UpdatePostTag(this PostTag postTag, PostTagViewModel postTagVM)
        {
            postTag.PostID = postTagVM.PostID;
            postTag.TagID = postTagVM.TagID;
        }

        public static void UpdatePost(this Post post, PostViewModel postVM)
        {
            post.Name = postVM.Name;
            post.Alias = postVM.Alias;
            post.CategoryID = postVM.CategoryID;
            post.Image = postVM.Image;
            post.Description = postVM.Description;
            post.Content = postVM.Content;
            post.HomeFlag = postVM.HomeFlag;
            post.HotFlag = postVM.HotFlag;
            post.ViewCount = postVM.ViewCount;
            post.CreatedDate = postVM.CreatedDate;
            post.CreatedBy = postVM.CreatedBy;
            post.UpdateDate = postVM.UpdateDate;
            post.UpdateBy = postVM.UpdateBy;
            post.MetaKeyword = postVM.MetaKeyword;
            post.MetaDescription = postVM.MetaDescription;
            post.Status = postVM.Status;
        }

        public static void UpdateTag(this Tag tag, TagViewModel tagVM)
        {
            tag.Name = tagVM.Name;
            tag.Type = tagVM.Type;
        }

        public static void UpdateProduct(this Product product, ProductViewModel productVM)
        {
            product.Name = productVM.Name;
            product.Alias = productVM.Alias;
            product.ProductCategoryID = productVM.ProductCategoryID;
            product.Image = productVM.Image;
            product.MoreImage = productVM.MoreImage;
            product.Price = productVM.Price;
            product.PromotionPrice = productVM.PromotionPrice;
            product.Warranty = productVM.Warranty;
            product.Description = productVM.Description;
            product.Content = productVM.Content;
            product.HomeFlag = productVM.HomeFlag;
            product.HotFlag = productVM.HotFlag;
            product.ViewCount = productVM.ViewCount;
            product.CreatedDate = productVM.CreatedDate;
            product.CreatedBy = productVM.CreatedBy;
            product.UpdateDate = productVM.UpdateDate;
            product.UpdateBy = productVM.UpdateBy;
            product.MetaKeyword = productVM.MetaKeyword;
            product.MetaDescription = productVM.MetaDescription;
            product.Status = productVM.Status;
            product.CarModel = productVM.CarModel;
        }

        public static void UpdateOrderDetail(this OrderDetail orderDetail, OrderDetailViewModel orderDetailVm)
        {
            orderDetail.ProductID = orderDetailVm.ProductID;
            orderDetail.Quantity = orderDetailVm.Quantity;
        }

        public static void UpdateProductCategory(this ProductCategory productCategory, ProductCategoryViewModel productCategoryVm)
        {
            productCategory.Name = productCategoryVm.Name;
            productCategory.Alias = productCategoryVm.Alias;
            productCategory.Description = productCategoryVm.Description;
            productCategory.ParentID = productCategoryVm.ParentID;
            productCategory.DisplayOrder = productCategoryVm.DisplayOrder;
            productCategory.Image = productCategoryVm.Image;
            productCategory.HomeFlag = productCategoryVm.HomeFlag;
            productCategory.CreatedDate = productCategoryVm.CreatedDate;
            productCategory.CreatedBy = productCategoryVm.CreatedBy;
            productCategory.UpdateDate = productCategoryVm.UpdateDate;
            productCategory.UpdateBy = productCategoryVm.UpdateBy;
            productCategory.MetaKeyword = productCategoryVm.MetaKeyword;
            productCategory.MetaDescription = productCategoryVm.MetaDescription;
            productCategory.Status = productCategoryVm.Status;
        }

        public static void UpdateSupportOnline(this SupportOnline supportOnline, SupportOnlineViewModel supportOnlineVm)
        {
            supportOnline.Name = supportOnlineVm.Name;
            supportOnline.Department = supportOnlineVm.Department;
            supportOnline.Skype = supportOnlineVm.Skype;
            supportOnline.Mobile = supportOnlineVm.Mobile;
            supportOnline.Email = supportOnlineVm.Email;
            supportOnline.Yahoo = supportOnlineVm.Yahoo;
            supportOnline.Status = supportOnlineVm.Status;
            supportOnline.DisplayOrder = supportOnlineVm.DisplayOrder;
    }
    }
}