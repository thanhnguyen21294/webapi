using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using Service;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace Web.Web
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            container.RegisterType<IPostCategoryService, PostCategoryService>();
            
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}