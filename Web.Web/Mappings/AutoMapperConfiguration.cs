﻿using AutoMapper;
using Model.Models;
using Web.Web.Controllers;
using Web.Web.Models;

namespace Web.Web.Mappings
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Post, PostViewModel>();
                cfg.CreateMap<PostTag, PostTagViewModel>();
                cfg.CreateMap<PostCategory, PostCategoryViewModel>().ForMember(dest=>dest.Posts, opt=>opt.Ignore());
                cfg.CreateMap<Tag, TagViewModel>();
                cfg.CreateMap<Product, ProductViewModel>();
                cfg.CreateMap<OrderDetail, OrderDetailViewModel>();
                cfg.CreateMap<ProductCategory, ProductCategoryViewModel>();
                cfg.CreateMap<SupportOnline, SupportOnlineViewModel>();
            });
        }
    }
}