﻿using AutoMapper;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Web.Web.Infrastructure.Core;
using Web.Web.Infrastructure.Extensions;
using Web.Web.Models;

namespace Web.Web.Controllers
{
    [RoutePrefix("api/supportonline")]
    public class SupportOnlineController : ApiControllerBase
    {
        private ISupportOnlineService _supportOnlineService;

        public SupportOnlineController(IErrorService errorService, ISupportOnlineService supportOnlineService): base(errorService)
        {
            _supportOnlineService = supportOnlineService;
        }
        [Route("add")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, SupportOnlineViewModel supportOnlineVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newSupportOnline = new SupportOnline();
                    newSupportOnline.UpdateSupportOnline(supportOnlineVm);
                    var model = _supportOnlineService.Add(newSupportOnline);
                    _supportOnlineService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, model);
                }
                return response;
            });
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var list = _supportOnlineService.GetAll();

                var listVm = Mapper.Map<IEnumerable<SupportOnline>, IEnumerable<SupportOnlineViewModel>>(list);

                response = request.CreateResponse(HttpStatusCode.OK, listVm);

                return response;

            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var model = _supportOnlineService.GetById(id);

                var modelVm = Mapper.Map<SupportOnline, SupportOnlineViewModel>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        public HttpResponseMessage Update(HttpRequestMessage request, SupportOnlineViewModel supportOnlineVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newSupportOnline = _supportOnlineService.GetById(supportOnlineVm.ID);
                    newSupportOnline.UpdateSupportOnline(supportOnlineVm);
                    _supportOnlineService.Update(newSupportOnline);
                    _supportOnlineService.Save();
                    var responseData = Mapper.Map<SupportOnline, SupportOnlineViewModel>(newSupportOnline);
                    response = request.CreateResponse(HttpStatusCode.Created);                    
                }
                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _supportOnlineService.Delete(id);
                    _supportOnlineService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }
                return response;
            });
        }
    }
}
