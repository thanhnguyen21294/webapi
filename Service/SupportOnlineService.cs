﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface ISupportOnlineService
    {
        SupportOnline Add(SupportOnline supportOnline);
        void Update(SupportOnline supportOnline);
        IEnumerable<SupportOnline> GetAll();
        SupportOnline GetById(int id);
        SupportOnline Delete(int id);
        void Save();
    }
    public class SupportOnlineService : ISupportOnlineService
    {
        private ISupportOnlineRepository _supportOnlineRepository;
        private IUnitOfWork _unitOfWork;
        public SupportOnlineService(ISupportOnlineRepository supportOnlineRepository, IUnitOfWork unitOfWork){
            this._supportOnlineRepository = supportOnlineRepository;
            this._unitOfWork = unitOfWork;
        }
        public SupportOnline Add(SupportOnline supportOnline)
        {
            return _supportOnlineRepository.Add(supportOnline);
        }

        public SupportOnline Delete(int id)
        {
            return _supportOnlineRepository.Delete(id);
        }

        public IEnumerable<SupportOnline> GetAll()
        {
            return _supportOnlineRepository.GetAll();
        }

        public SupportOnline GetById(int id)
        {
            return _supportOnlineRepository.GetSingleById(id);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(SupportOnline supportOnline)
        {
            _supportOnlineRepository.Update(supportOnline);
        }
    }
}
